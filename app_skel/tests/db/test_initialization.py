# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import TestCase
from unittest import mock

from ...db import initialization


class TestDBInitialization(TestCase):

    @mock.patch.object(initialization, 'get_engine')
    @mock.patch.object(initialization, 'Base')
    def test_init_without_migrations(self, mock_base, mock_get_engine):
        engine = mock.MagicMock()
        mock_get_engine.return_value = engine
        initialization.init_without_migrations()
        mock_get_engine.assert_called_once()
        mock_base.metadata.create_all.assert_called_once_with(bind=engine)
