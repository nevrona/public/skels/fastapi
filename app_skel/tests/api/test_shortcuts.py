# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from unittest import TestCase
from unittest import mock

from sqlalchemy.orm.exc import MultipleResultsFound

from ...api import shortcuts


class TestAPIShortcuts(TestCase):

    def test_get_db(self):
        db = mock.MagicMock()
        request = mock.MagicMock()
        request.state.db = db
        self.assertEqual(shortcuts.get_db(request), db)

    @mock.patch.object(shortcuts, 'get_object_crud_or_404')
    def test_get_object_or_404(self, mock_get_object_crud_or_404):
        crud = mock.MagicMock()
        crud.db_obj = None
        mock_get_object_crud_or_404.return_value = crud
        params = mock.MagicMock(), mock.MagicMock(), mock.MagicMock()

        async def async_test():
            self.assertIsNone(await shortcuts.get_object_or_404(*params))
            mock_get_object_crud_or_404.assert_called_once_with(*params, None, None)

        asyncio.run(async_test())

    @mock.patch.object(shortcuts, 'HTTPException')
    @mock.patch.object(shortcuts, 'get_object_crud_or_httpexception')
    def test_get_object_crud_or_404(
            self,
            mock_get_object_crud_or_httpexception,
            mock_httpexception,
    ):
        mock_get_object_crud_or_httpexception.return_value = None
        klass = mock.MagicMock()
        klass.model.__name__ = 'santiago maldonado'
        params = mock.MagicMock(), klass, mock.MagicMock()

        async def async_test():
            self.assertIsNone(await shortcuts.get_object_crud_or_404(*params))
            mock_httpexception.assert_called_once_with(
                status_code=404,
                detail='Santiago Maldonado not found',
            )
            mock_get_object_crud_or_httpexception.assert_called_once_with(
                mock_httpexception(),
                *params,
                None,
                None,
            )

        asyncio.run(async_test())

    def test_get_object_crud_or_httpexception(self):
        db = mock.MagicMock(name='DB')
        exc = mock.MagicMock(name='HTTPException')
        filters = [mock.MagicMock()]

        # MagicMock can't be used with await so I need a custom testing class
        klass_mock = mock.MagicMock()

        class Klass:

            def __init__(self, *args):
                klass_mock(*args)

            async def read(self, **kwargs):
                klass_mock.read(**kwargs)

        klass = Klass

        async def async_test():
            result = await shortcuts.get_object_crud_or_httpexception(
                exc,
                db,
                klass,
                filters,
            )
            self.assertIsInstance(
                result,
                Klass,
            )
            klass_mock.assert_called_once_with(db)
            klass_mock.read.assert_called_once_with(
                one=True,
                filters=filters,
                filter_combination=None,
                joins=None,
            )

        asyncio.run(async_test())

    def test_get_object_crud_or_httpexception_raise_exc(self):
        db = mock.MagicMock(name='DB')
        klass = mock.MagicMock(name='Class')
        exc = ValueError()
        filters = [mock.MagicMock()]
        klass.return_value.read.side_effect = MultipleResultsFound()

        async def async_test():
            with self.assertRaises(ValueError):
                await shortcuts.get_object_crud_or_httpexception(
                    exc,
                    db,
                    klass,
                    filters,
                )
            klass.assert_called_once_with(db)
            klass.return_value.read.assert_called_once_with(
                one=True,
                filters=filters,
                filter_combination=None,
                joins=None,
            )

        asyncio.run(async_test())
