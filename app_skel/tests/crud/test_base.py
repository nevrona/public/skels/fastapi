# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from unittest import TestCase
from unittest import mock

from sqlalchemy.exc import ArgumentError

from ...crud import base


class TestCrudBase(TestCase):

    def setUp(self) -> None:
        magic_model = mock.MagicMock

        class CrudTest(base.Crud):
            model = magic_model

        self.crud_class = CrudTest
        self.db = mock.MagicMock()
        self.crud = self.crud_class(self.db)

    def test_crud_init(self):
        self.assertIsInstance(self.crud, self.crud_class)
        self.assertRaises(AttributeError, base.Crud, self.db)

    def test_db_obj(self):
        self.assertIsNone(self.crud.db_obj)
        obj = mock.MagicMock()
        self.crud.db_obj = obj
        self.assertEqual(self.crud.db_obj, obj)

    def test_create(self):

        async def async_test():
            obj = await self.crud.create(id=1)
            self.assertIsInstance(obj, self.crud_class.model)
            self.crud.db.add.assert_called_once_with(obj)
            self.crud.db.commit.assert_called_once()
            self.crud.db.refresh.assert_called_once_with(obj)

        asyncio.run(async_test())

    def test_update_no_obj(self):

        async def async_test():
            with self.assertRaises(ValueError):
                await self.crud.update(id=1)

        asyncio.run(async_test())

    def test_update_obj_no_data(self):
        obj = mock.MagicMock()
        obj.id = 0

        async def async_test():
            # No data so expect no change
            self.crud.db_obj = obj
            updated_obj = await self.crud.update()
            self.assertEqual(updated_obj, obj)
            self.assertEqual(updated_obj.id, 0)
            self.crud.db.add.assert_called_once_with(updated_obj)
            self.crud.db.commit.assert_called_once()
            self.crud.db.refresh.assert_called_once_with(updated_obj)

        asyncio.run(async_test())

    def test_update_obj_with_data(self):
        obj = mock.MagicMock()
        obj.id = 0

        async def async_test():
            # Change expected
            self.crud.db_obj = obj
            updated_obj = await self.crud.update(id=1)
            self.assertEqual(updated_obj, obj)
            self.assertEqual(updated_obj.id, 1)
            self.crud.db.add.assert_called_once_with(updated_obj)
            self.crud.db.commit.assert_called_once()
            self.crud.db.refresh.assert_called_once_with(updated_obj)

        asyncio.run(async_test())

    def test_delete_no_obj(self):

        async def async_test():
            with self.assertRaises(ValueError):
                await self.crud.delete()

        asyncio.run(async_test())

    def test_delete_obj(self):
        obj = mock.MagicMock()

        async def async_test():
            self.crud.db_obj = obj
            deleted_obj = await self.crud.delete()
            self.assertEqual(deleted_obj, obj)
            self.crud.db.delete.assert_called_once_with(deleted_obj)
            self.crud.db.commit.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_defaults(self):

        async def async_test():
            values = await self.crud.read()
            self.assertIsInstance(values, mock.MagicMock)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query().offset.assert_called_once()
            self.crud.db.query().offset().limit.assert_called_once()
            self.crud.db.query().offset().limit().all.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_offset(self):

        async def async_test():
            values = await self.crud.read(offset=1)
            self.assertIsInstance(values, mock.MagicMock)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query().offset.assert_called_once_with(1)
            self.crud.db.query().offset().limit.assert_called_once()
            self.crud.db.query().offset().limit().all.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_limit(self):

        async def async_test():
            values = await self.crud.read(limit=1)
            self.assertIsInstance(values, mock.MagicMock)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query().offset().limit.assert_called_once()
            self.crud.db.query().offset().limit.assert_called_once_with(1)
            self.crud.db.query().offset().limit().all.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_query(self):
        query = mock.MagicMock()

        async def async_test():
            values = await self.crud.read(query=query)
            self.assertIsInstance(values, mock.MagicMock)
            self.crud.db.query.assert_not_called()
            query.offset.assert_called_once()
            query.offset().limit.assert_called_once()
            query.offset().limit().all.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_joins(self):
        joins = [mock.MagicMock(), mock.MagicMock()]

        async def async_test():
            values = await self.crud.read(joins=joins)
            self.assertIsInstance(values, mock.MagicMock)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query().join.assert_called_once_with(joins[0])
            self.crud.db.query().join().join.assert_called_once_with(joins[1])
            self.crud.db.query().join().join().offset.assert_called_once()
            self.crud.db.query().join().join().offset().limit.assert_called_once()
            self.crud.db.query().join().join().offset().limit().all.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_filters_combination_default(self):
        filters = [mock.MagicMock(), mock.MagicMock()]

        async def async_test():
            with self.assertRaises(ArgumentError):
                await self.crud.read(filters=filters)

        asyncio.run(async_test())

    def test_read_param_filters(self):
        filters = [mock.MagicMock(), mock.MagicMock()]
        filter_combination = mock.MagicMock()

        async def async_test():
            values = await self.crud.read(
                filters=filters,
                filter_combination=filter_combination,
            )
            self.assertIsInstance(values, mock.MagicMock)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query.return_value.filter.assert_called_once_with(
                filter_combination(filters[0], filters[1]),
            )
            self.crud.db.query().filter().offset.assert_called_once()
            self.crud.db.query().filter().offset().limit.assert_called_once()
            self.crud.db.query().filter().offset().limit().all.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_return_query(self):

        async def async_test():
            query = await self.crud.read(return_query=True)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.assertEqual(query, self.db.query(self.crud.model))

        asyncio.run(async_test())

    def test_read_param_one_or_none(self):
        self.crud.db.query.return_value.one_or_none.return_value = None

        async def async_test():
            value = await self.crud.read(one_or_none=True)
            self.assertIsNone(value)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query.return_value.one_or_none.assert_called_once()

        asyncio.run(async_test())

    def test_read_param_one(self):
        expected = mock.MagicMock()
        self.crud.db.query.return_value.one.return_value = expected

        async def async_test():
            value = await self.crud.read(one=True)
            self.assertEqual(value, expected)
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query.return_value.one.assert_called_once()

        asyncio.run(async_test())

    def tests_delete_bulk_param_filters_combination_default(self):
        filters = [mock.MagicMock(), mock.MagicMock()]

        async def async_test():
            with self.assertRaises(ArgumentError):
                await self.crud.delete_bulk(filters=filters)

        asyncio.run(async_test())

    def tests_delete_bulk_param_filters(self):
        filters = [mock.MagicMock(), mock.MagicMock()]
        filter_combination = mock.MagicMock()

        async def async_test():
            await self.crud.delete_bulk(
                filters=filters,
                filter_combination=filter_combination,
            )
            self.crud.db.query.assert_called_once_with(self.crud.model)
            self.crud.db.query.return_value.filter.assert_called_once_with(
                filter_combination(filters[0], filters[1]),
            )
            self.crud.db.query.return_value.filter().delete.assert_called_once()
            self.crud.db.commit.assert_called_once()

        asyncio.run(async_test())
