# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from unittest import TestCase
from unittest import mock

from ...app import events


class TestAppEvents(TestCase):

    @mock.patch.object(events, 'settings')
    @mock.patch.object(events, 'logger')
    def test_startup(self, mock_logger, mock_settings):
        mock_settings.DEVELOPMENT_MODE = False

        async def async_test():
            await events.startup()
            mock_logger.info.assert_called_once()
            mock_logger.debug.assert_called_once()

        asyncio.run(async_test())

    @mock.patch.object(events, 'settings')
    @mock.patch.object(events, 'logger')
    def test_startup_dev_mode(self, mock_logger, mock_settings):
        mock_settings.DEVELOPMENT_MODE = True

        async def async_test():
            await events.startup()
            mock_logger.info.assert_called_once()
            mock_logger.debug.assert_called_once()
            mock_logger.warning.assert_called_once()

        asyncio.run(async_test())

    def test_shutdown(self):

        async def async_test():
            await events.shutdown()

        asyncio.run(async_test())
