# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import TestCase
from unittest import mock

from ...app import fastapi


class TestAppFastapi(TestCase):

    @mock.patch.object(fastapi, 'get_project_internal_version')
    @mock.patch.object(fastapi, 'get_project_version')
    @mock.patch.object(fastapi, 'init_without_migrations')
    @mock.patch.object(fastapi, 'settings')
    @mock.patch.object(fastapi, 'FastAPI')
    def test_create_app(
            self,
            mock_fastapi,
            mock_settings,
            mock_init_without_migrations,
            mock_get_project_version,
            mock_get_project_internal_version,
    ):
        mock_settings.APP_DESCRIPTION = 'description'
        mock_settings.DOCS_URL_PATH = '/docs'
        mock_settings.APP_TITLE = 'test'
        mock_settings.OPENAPI_URL_PATH = '/openapi'
        mock_settings.DEBUG = False
        mock_settings.DEVELOPMENT_MODE = False
        mock_get_project_version.return_value = '1.2.3'
        mock_get_project_internal_version.return_value = 'internal-version'

        app = fastapi.create_app()
        self.assertEqual(app, mock_fastapi.return_value)
        mock_init_without_migrations.assert_called_once_with()
        mock_get_project_version.assert_called()
        mock_get_project_internal_version.assert_not_called()
        mock_fastapi.assert_called_once_with(
            title='test',
            description='description v1.2.3',
            version='1.2.3',
            debug=False,
            openapi_url='/openapi',
            docs_url=None,
            redoc_url=None,
            swagger_ui_oauth2_redirect_url='/docs/oauth2-redirect',
        )

    @mock.patch.object(fastapi, 'get_project_internal_version')
    @mock.patch.object(fastapi, 'get_project_version')
    @mock.patch.object(fastapi, 'init_without_migrations')
    @mock.patch.object(fastapi, 'settings')
    @mock.patch.object(fastapi, 'FastAPI')
    def test_create_app_debug_mode(
            self,
            mock_fastapi,
            mock_settings,
            mock_init_without_migrations,
            mock_get_project_version,
            mock_get_project_internal_version,
    ):
        mock_settings.APP_DESCRIPTION = 'description'
        mock_settings.DOCS_URL_PATH = '/docs'
        mock_settings.APP_TITLE = 'test'
        mock_settings.OPENAPI_URL_PATH = '/openapi'
        mock_settings.DEBUG = True
        mock_settings.DEVELOPMENT_MODE = False
        mock_get_project_version.return_value = '1.2.3'
        mock_get_project_internal_version.return_value = 'internal-version'

        app = fastapi.create_app()
        self.assertEqual(app, mock_fastapi.return_value)
        mock_init_without_migrations.assert_called_once_with()
        mock_get_project_version.assert_called()
        mock_get_project_internal_version.assert_called_once()
        mock_fastapi.assert_called_once_with(
            title='test',
            description='description v1.2.3 (internal-version)',
            version='1.2.3',
            debug=True,
            openapi_url='/openapi',
            docs_url=None,
            redoc_url=None,
            swagger_ui_oauth2_redirect_url='/docs/oauth2-redirect',
        )

    @mock.patch.object(fastapi, 'get_project_internal_version')
    @mock.patch.object(fastapi, 'get_project_version')
    @mock.patch.object(fastapi, 'init_without_migrations')
    @mock.patch.object(fastapi, 'settings')
    @mock.patch.object(fastapi, 'FastAPI')
    def test_create_app_dev_mode(
            self,
            mock_fastapi,
            mock_settings,
            mock_init_without_migrations,
            mock_get_project_version,
            mock_get_project_internal_version,
    ):
        mock_settings.APP_DESCRIPTION = 'description'
        mock_settings.DOCS_URL_PATH = '/docs'
        mock_settings.APP_TITLE = 'test'
        mock_settings.OPENAPI_URL_PATH = '/openapi'
        mock_settings.DEBUG = True
        mock_settings.DEVELOPMENT_MODE = True
        mock_get_project_version.return_value = '1.2.3'
        mock_get_project_internal_version.return_value = 'internal-version'

        app = fastapi.create_app()
        self.assertEqual(app, mock_fastapi.return_value)
        mock_init_without_migrations.assert_called_once_with()
        mock_get_project_version.assert_called()
        mock_get_project_internal_version.assert_called_once()
        mock_fastapi.assert_called_once_with(
            title='test',
            description='description v1.2.3 (internal-version) (DEV MODE)',
            version='1.2.3',
            debug=True,
            openapi_url='/openapi',
            docs_url=None,
            redoc_url=None,
            swagger_ui_oauth2_redirect_url='/docs/oauth2-redirect',
        )
