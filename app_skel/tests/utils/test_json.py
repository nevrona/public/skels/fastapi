# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""JSON utils tests.

These tests were extracted from the Django project and expanded by us.
"""

# This file was extracted from Django
# https://github.com/django/django/blob/312049091288dbba2299de8d07ea3e3311ed7238/tests/serializers/test_json.py

# Copyright (c) Django Software Foundation and individual contributors.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
#     1. Redistributions of source code must retain the above copyright notice,
#        this list of conditions and the following disclaimer.
#
#     2. Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#
#     3. Neither the name of Django nor the names of its contributors may be used
#        to endorse or promote products derived from this software without
#        specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import datetime
import json
from decimal import Decimal
from unittest import TestCase
from unittest import mock

from ...utils.functional import lazystr
from ...utils.json import DjangoJSONEncoder
from ...utils.json import JSONEncoder

EAT = datetime.timezone(datetime.timedelta(seconds=10800), '+0300')
CNT = datetime.timezone(datetime.timedelta(seconds=0), '+0000')


class TestUtilsDjangoJSONEncoder(TestCase):

    def setUp(self) -> None:
        self.encoder_class = DjangoJSONEncoder

    def test_can_not_serialize(self):
        self.assertRaises(
            TypeError,
            json.dumps,
            mock.MagicMock(),
            cls=self.encoder_class,
        )

    def test_timedelta(self):
        duration = datetime.timedelta(days=1, hours=2, seconds=3)
        self.assertEqual(
            json.dumps({'duration': duration}, cls=self.encoder_class),
            '{"duration": "P1DT02H00M03S"}',
        )
        duration = datetime.timedelta(0)
        self.assertEqual(
            json.dumps({'duration': duration}, cls=self.encoder_class),
            '{"duration": "P0DT00H00M00S"}',
        )

    def test_datetime(self):
        some_datetime = datetime.datetime(year=2020, month=3, day=27, hour=2, second=3,
                                          microsecond=999999)
        self.assertEqual(
            json.dumps({'datetime': some_datetime}, cls=self.encoder_class),
            '{"datetime": "2020-03-27T02:00:03.999"}',
        )

    def test_datetime_with_tz(self):
        some_datetime = datetime.datetime(year=2020, month=3, day=27, hour=2, second=3,
                                          microsecond=999999, tzinfo=CNT)
        self.assertEqual(
            json.dumps({'datetime': some_datetime}, cls=self.encoder_class),
            '{"datetime": "2020-03-27T02:00:03.999Z"}',
        )

    def test_time(self):
        some_time = datetime.time(hour=2, second=3)
        self.assertEqual(
            json.dumps({'time': some_time}, cls=self.encoder_class),
            '{"time": "02:00:03"}',
        )

    def test_time_with_microseconds(self):
        some_time = datetime.time(hour=2, second=3, microsecond=999999)
        self.assertEqual(
            json.dumps({'time': some_time}, cls=self.encoder_class),
            '{"time": "02:00:03.999"}',
        )

    def test_time_timezone_aware_exception(self):
        some_time = datetime.time(hour=2, second=3, tzinfo=EAT)
        self.assertRaises(
            ValueError,
            json.dumps,
            {'time': some_time},
            cls=self.encoder_class,
        )

    def test_date(self):
        some_date = datetime.date(year=2020, month=3, day=27)
        self.assertEqual(
            json.dumps({'date': some_date}, cls=self.encoder_class),
            '{"date": "2020-03-27"}',
        )

    def test_decimal(self):
        dec = Decimal(1)
        self.assertEqual(
            json.dumps({'dec': dec}, cls=self.encoder_class),
            '{"dec": "1"}',
        )

    def test_lazy_string_encoding(self):
        self.assertEqual(
            json.dumps({'lang': lazystr('French')}, cls=self.encoder_class),
            '{"lang": "French"}',
        )


class TestUtilsJSONEncoder(TestUtilsDjangoJSONEncoder):

    def setUp(self) -> None:
        self.encoder_class = JSONEncoder
