# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
from unittest import TestCase

from ...utils import duration


class TestUtilsISODurationString(TestCase):

    def test_simple(self):
        value = datetime.timedelta(hours=1, minutes=3, seconds=5)
        self.assertEqual(duration.duration_iso_string(value), 'P0DT01H03M05S')

    def test_days(self):
        value = datetime.timedelta(days=1, hours=1, minutes=3, seconds=5)
        self.assertEqual(duration.duration_iso_string(value), 'P1DT01H03M05S')

    def test_microseconds(self):
        value = datetime.timedelta(hours=1, minutes=3, seconds=5, microseconds=12345)
        self.assertEqual(duration.duration_iso_string(value), 'P0DT01H03M05.012345S')

    def test_negative(self):
        value = -1 * datetime.timedelta(days=1, hours=1, minutes=3, seconds=5)
        self.assertEqual(duration.duration_iso_string(value), '-P1DT01H03M05S')
