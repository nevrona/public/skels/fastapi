# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import TestCase

from ...utils import base64


class TestUtilsBase64(TestCase):

    def test_b64encode_strips_padding(self):
        encoded = base64.b64encode(b'abcd')
        self.assertEqual(encoded, b'YWJjZA')

    def test_b64encode_no_padding_required(self):
        encoded = base64.b64encode(b'abc')
        self.assertEqual(encoded, b'YWJj')

    def test_b64decode_stripped_padding_works(self):
        decoded = base64.b64decode(b'YWJjZA')
        self.assertEqual(decoded, b'abcd')

    def test_b64decode_no_padding_required(self):
        decoded = base64.b64decode(b'YWJj')
        self.assertEqual(decoded, b'abc')

    def test_b64decode_accepts_strings(self):
        decoded = base64.b64decode('YWJj')
        self.assertEqual(decoded, b'abc')

    def test_b64decode_works_with_padding(self):
        decoded = base64.b64decode(b'YWJj==')
        self.assertEqual(decoded, b'abc')
