# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import TestCase
from unittest import mock

from ...conf import helpers


class TestConfHelpers(TestCase):

    def test_unfroze_settings_class(self):
        settings = mock.MagicMock()
        helpers.unfroze_settings_class(settings)
        self.assertEqual(settings.Config.allow_mutation, True)

    def test_froze_settings(self):
        settings = mock.MagicMock()
        helpers.froze_settings(settings)
        self.assertEqual(settings.Config.allow_mutation, False)

    def test_get_logging_config(self):
        settings = mock.MagicMock()
        settings.PACKAGE_NAME = 'package'
        settings.LOGLEVEL = 'level'
        settings.LOGGING = {'loggers': {'package': {}}}
        config = helpers.get_logging_config(settings)
        self.assertEqual(config, {'loggers': {'package': {'level': 'level'}}})

    @mock.patch.object(helpers, 'logging')
    @mock.patch.object(helpers, 'get_logging_config')
    def test_set_python_logging_config(
            self,
            mock_get_logging_config,
            mock_logging,
    ):
        settings = mock.MagicMock()
        helpers.set_python_logging_config(settings)
        mock_get_logging_config.assert_called_once_with(settings)
        mock_logging.config.dictConfig.assert_called_once_with(
            mock_get_logging_config.return_value,
        )
