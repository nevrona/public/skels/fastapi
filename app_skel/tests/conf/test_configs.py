# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from unittest import TestCase
from unittest import mock

from ...conf import configs


class TestConfConfigs(TestCase):

    @mock.patch.object(configs, 'TestSettings')
    @mock.patch.object(configs, 'get_project_settings')
    def test_get_test_settings(self, mock_get_project_settings, mock_test_settings):
        settings = configs.get_test_settings()
        self.assertEqual(settings, mock_get_project_settings.return_value)
        mock_get_project_settings.assert_called_once_with(
            **mock_test_settings.return_value.dict.return_value,
        )

    @mock.patch.object(configs, 'os')
    @mock.patch.object(configs, 'get_project_settings')
    def test_get_settings_project(self, mock_get_project_settings, mock_os):
        mock_os.getenv.return_value = 'false'

        settings = configs.get_settings.__wrapped__()  # Bypass lru_cache
        self.assertEqual(settings, mock_get_project_settings.return_value)
        mock_os.getenv.assert_called_once()
        mock_get_project_settings.assert_called_once()

    @mock.patch.object(configs, 'os')
    @mock.patch.object(configs, 'get_test_settings')
    def test_get_settings_test_env_value_true(self, mock_get_test_settings, mock_os):
        mock_os.getenv.return_value = 'true'

        settings = configs.get_settings.__wrapped__()  # Bypass lru_cache
        self.assertEqual(settings, mock_get_test_settings.return_value)
        mock_os.getenv.assert_called_once()
        mock_get_test_settings.assert_called_once()

    @mock.patch.object(configs, 'os')
    @mock.patch.object(configs, 'get_test_settings')
    def test_get_settings_test_env_value_1(self, mock_get_test_settings, mock_os):
        mock_os.getenv.return_value = '1'

        settings = configs.get_settings.__wrapped__()  # Bypass lru_cache
        self.assertEqual(settings, mock_get_test_settings.return_value)
        mock_os.getenv.assert_called_once()
        mock_get_test_settings.assert_called_once()

    @mock.patch.object(configs, 'set_python_logging_config')
    @mock.patch.object(configs, 'LocalSettings')
    @mock.patch.object(configs, 'Settings')
    def test_get_project_settings_no_local_settings(
            self,
            mock_settings_class,
            mock_local_settings_class,
            mock_set_python_logging_config,
    ):
        mock_local_settings_class.side_effect = None

        settings = configs.get_project_settings(a=1, b=2)
        mock_settings_class.assert_called_once_with(a=1, b=2)
        self.assertEqual(settings, mock_settings_class.return_value)
        mock_set_python_logging_config.assert_called_once_with(
            mock_settings_class.return_value,
        )

    @mock.patch.object(configs, 'set_python_logging_config')
    @mock.patch.object(configs, 'LocalSettings')
    @mock.patch.object(configs, 'Settings')
    def test_get_project_settings_with_local_settings(
            self,
            mock_settings_class,
            mock_local_settings_class,
            mock_set_python_logging_config,
    ):
        mock_local_settings_class.return_value.dict.return_value = {'c': 3, 'a': 3}

        settings = configs.get_project_settings(use_local_settings=True, a=1, b=2)
        mock_settings_class.assert_called_once_with(c=3, a=1, b=2)
        self.assertEqual(settings, mock_settings_class.return_value)
        mock_set_python_logging_config.assert_called_once_with(
            mock_settings_class.return_value,
        )

    @mock.patch.object(configs, 'set_python_logging_config')
    @mock.patch.object(configs, 'LocalSettings')
    @mock.patch.object(configs, 'Settings')
    def test_get_project_settings_with_local_settings_but_dont_use_them(
            self,
            mock_settings_class,
            mock_local_settings_class,
            mock_set_python_logging_config,
    ):
        mock_local_settings_class.return_value.dict.return_value = {'c': 3, 'a': 3}

        settings = configs.get_project_settings(use_local_settings=False, a=1, b=2)
        mock_settings_class.assert_called_once_with(a=1, b=2)
        self.assertEqual(settings, mock_settings_class.return_value)
        mock_set_python_logging_config.assert_called_once_with(
            mock_settings_class.return_value,
        )
