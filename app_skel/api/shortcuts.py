# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Helper functions and classes definition for API endpoints."""

import functools
import typing

from fastapi import HTTPException
from fastapi import Request
from fastapi import status
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound

from ..crud.base import Crud


def get_db(request: Request) -> Session:
    """Db session dependency for FastAPI."""
    return request.state.db


async def get_object_crud_or_httpexception(
        exc: HTTPException,
        db: Session,
        klass: typing.Type[Crud],
        filters: typing.List[any],
        filter_combination=None,
        joins: typing.List[any] = None,
):
    """Retrieve object crud for single object or raise an exception.

    :param exc: Exception to raise when object not found.
    :param db: Db session.
    :param klass: Crud manager class.
    :param filters: Crud manager filters to retrieve a single item.
    :param filter_combination: [Optional] Filter combination function from
                               SQLAlchemy, such as `or_`, `and_`, etc (defaults
                               to `and_`).
    :param joins: [Optional] List of joins to apply to the query.

    :return: Db CRUD.

    :raise HTTPException: Db object not found.
    """
    manager = klass(db)
    try:
        await manager.read(
            one=True,
            filters=filters,
            filter_combination=filter_combination,
            joins=joins,
        )
    except (MultipleResultsFound, NoResultFound):
        raise exc

    return manager


async def get_object_crud_or_404(
        db: Session,
        klass: typing.Type[Crud],
        filters: typing.List[any],
        filter_combination=None,
        joins: typing.List[any] = None,
):
    """Retrieve object crud for single object or raise 404 HTTP exception.

    :param db: Db session.
    :param klass: Crud manager class.
    :param filters: Crud manager filters to retrieve a single item.
    :param filter_combination: [Optional] Filter combination function from
                               SQLAlchemy, such as `or_`, `and_`, etc (defaults
                               to `and_`).
    :param joins: [Optional] List of joins to apply to the query.

    :return: Db CRUD.

    :raise HTTPException: Db object not found.
    """
    return await get_object_crud_or_httpexception(
        HTTPException(
            status_code=404,
            detail=f'{klass.model.__name__.title()} not found',
        ),
        db,
        klass,
        filters,
        filter_combination,
        joins,
    )


async def get_object_or_404(
        db: Session,
        klass: typing.Type[Crud],
        filters: typing.List[any],
        filter_combination=None,
        joins: typing.List[any] = None,
):
    """Retrieve single object or raise 404 HTTP exception.

    :param db: Db session.
    :param klass: Crud manager class.
    :param filters: Crud manager filters to retrieve a single item.
    :param filter_combination: [Optional] Filter combination function from
                               SQLAlchemy, such as `or_`, `and_`, etc (defaults
                               to `and_`).
    :param joins: [Optional] List of joins to apply to the query.

    :return: Db object.

    :raise HTTPException: Db object not found.
    """
    crud = await get_object_crud_or_404(db, klass, filters, filter_combination, joins)
    return crud.db_obj


def catch(
        exc_classes: typing.Union[  # noqa: TAE002
            typing.Type[Exception],
            typing.Tuple[typing.Type[Exception], ...],
        ],
        *,
        status_code: int = status.HTTP_400_BAD_REQUEST,
) -> typing.Callable:
    """Catch any exception of given class/es raising an HTTPException instead."""

    def decorator(function):
        """Catch any exception raising an HTTPException instead."""

        @functools.wraps(function)
        async def wrapper(*args, **kwargs):
            """Wrap decorated function."""
            try:
                return await function(*args, **kwargs)
            except exc_classes as exc:
                detail = f'{exc}' or None
                raise HTTPException(status_code=status_code, detail=detail) from exc

        return wrapper

    return decorator
