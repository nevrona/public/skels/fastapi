# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Application main endpoints."""

from fastapi import APIRouter
from fastapi import Header
from fastapi import Response
from fastapi import status
from fastapi.openapi.docs import get_redoc_html
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.docs import get_swagger_ui_oauth2_redirect_html
from fastapi.responses import HTMLResponse

from .routing import build_prefix
from .routing import get_static
from ..conf import settings

router = APIRouter()


@router.get('/', status_code=status.HTTP_307_TEMPORARY_REDIRECT, include_in_schema=False)
def read_the_docs(response: Response, user_agent: str = Header('')) -> None:
    """Redirect to the documentation."""
    browsers = ('Firefox', 'Chrome', 'Chromium', 'Opera', 'MSIE', 'Safari')
    if any(browser in user_agent for browser in browsers):
        # Redirect human readable
        response.headers['Location'] = build_prefix(settings.DOCS_URL_PATH)
    else:
        # Redirect machine
        response.headers['Location'] = build_prefix(settings.OPENAPI_URL_PATH)


@router.get(settings.DOCS_URL_PATH, include_in_schema=False)
async def swagger_ui_html() -> HTMLResponse:
    """Get SwaggerUI HTML documents."""
    return get_swagger_ui_html(
        openapi_url=build_prefix(settings.OPENAPI_URL_PATH),
        title=f'{settings.APP_TITLE} - Swagger UI',
        swagger_js_url=get_static(['swagger', 'swagger-ui-bundle.js'], 'vendor'),
        swagger_css_url=get_static(['swagger', 'swagger-ui.css'], 'vendor'),
        swagger_favicon_url=get_static('favicon.png', 'imgs'),
        oauth2_redirect_url=build_prefix(f'{settings.DOCS_URL_PATH}/oauth2-redirect'),
    )


@router.get(f'{settings.DOCS_URL_PATH}/oauth2-redirect', include_in_schema=False)
async def swagger_ui_redirect_html() -> HTMLResponse:
    """Get SwaggerUI OAuth2 HTML."""
    return get_swagger_ui_oauth2_redirect_html()


@router.get(settings.REDOC_URL_PATH, include_in_schema=False)
async def redoc_html() -> HTMLResponse:
    """Get ReDoc HTML documents."""
    return get_redoc_html(
        openapi_url=build_prefix(settings.OPENAPI_URL_PATH),
        title=f'{settings.APP_TITLE} - ReDoc',
        redoc_js_url=get_static(['redoc', 'redoc.standalone.js'], 'vendor'),
        with_google_fonts=False,
        redoc_favicon_url=get_static('favicon.png', 'imgs'),
    )
