# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""API internal routing utilities."""

import os
import typing
from importlib import import_module
from pathlib import Path

from fastapi import APIRouter

from ..conf import settings


def build_prefix(api_prefix: str) -> str:
    """Build API URL prefix."""
    return f'{settings.API_PREFIX}{api_prefix}'


def find_and_add_endpoints(router: APIRouter, version: str) -> None:
    """Find endpoints for a given API version and add them to the router.

    Endpoints that begin with underscore will be ignored.

    :param router: Router to add the endpoints.
    :param version: A semver version string consisting solely in major and minor
                    values, i.e.: 1.2.
    """
    # Find and add endpoints
    version_path_name = f'v{version.replace(".", "_")}'
    endpoints_path = Path(os.path.join(settings.BASE_DIR, 'api', version_path_name,
                                       'endpoints'))
    for endpoint_path in endpoints_path.glob('*.py'):
        if endpoint_path.is_file() and not endpoint_path.name.startswith('_'):
            name = endpoint_path.name.replace(endpoint_path.suffix, '')
            module = import_module(f'.api.{version_path_name}.endpoints.{name}',
                                   settings.PACKAGE_NAME)
            endpoint_router = getattr(module, 'router')
            router.include_router(endpoint_router, prefix=f'/{name}', tags=[name])


def get_static(asset: typing.Union[str, typing.List[str]], category: str) -> str:
    """Get the URL path to an static asset.

    :param asset: The static asset or route to the static asset as a list.
    :param category: The asset category (such ass `css`, `js`, `vendor`, etc).
    """
    if isinstance(asset, str):
        asset_path = asset
    else:
        asset_path = '/'.join(asset)
    path = f'{settings.PACKAGE_NAME}/{category}/{asset_path}'

    if settings.STATIC_URL:
        return f'{settings.STATIC_URL}/{path}'

    return build_prefix(f'{settings.STATIC_URL_PATH}/{path}')
