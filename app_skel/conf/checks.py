# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Settings checks.

Set here your business requirements regarding settings.
"""

import typing

from .global_settings import Settings


def configure_settings(settings: Settings) -> None:
    """Apply business logic to settings."""
    # Process dev mode
    if settings.DEVELOPMENT_MODE:
        settings.DEBUG = True
        settings.LOGLEVEL = 'DEBUG'
        settings.ALLOW_FRONTEND_LOCAL = True
        settings.ENABLE_HTTPS_REDIRECT = False

    # Set the allowed origins list
    allowed_origins: typing.List[str] = [
        'http://127.0.0.1:3000',  # Frontend, for development
    ] if settings.ALLOW_FRONTEND_LOCAL else []
    # HTTPS protocol hardcoded :)
    allowed_origins.extend(f'https://{host}' for host in settings.ALLOWED_HOSTS)
    settings.ALLOWED_ORIGINS = allowed_origins

    # Set SQLAlchemy Database URI
    if settings.DATABASE_USER:
        # see: https://www.cockroachlabs.com/docs/stable/connection-parameters.html
        if settings.DATABASE_PASSWORD:
            local_part = f'{settings.DATABASE_USER}:{settings.DATABASE_PASSWORD}'
        else:
            local_part = settings.DATABASE_USER
        database_uri: str = (
            f'{settings.DATABASE_DIALECT}://{local_part}@'
            f'{settings.DATABASE_HOST}:{settings.DATABASE_PORT}/'
            f'{settings.DATABASE_NAME}?{settings.DATABASE_PARAMS}'
        )
    else:
        database_uri: str = f'sqlite:///./{settings.DATABASE_NAME}'
    settings.DATABASE_URI = database_uri
