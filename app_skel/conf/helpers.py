# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Configuration helpers."""

import logging.config
import typing

from .global_settings import Settings


def unfroze_settings_class(klass: typing.Type[Settings]) -> None:
    """Unfroze given settings class to allow changes after instantiated."""
    klass.Config.allow_mutation = True


def froze_settings(settings: Settings) -> None:
    """Froze given settings to avoid further unwanted changes."""
    settings.Config.allow_mutation = False  # Froze settings


def get_logging_config(settings: Settings) -> dict:
    """Get the logging configuration for the Python logger as dict."""
    dict_config = settings.LOGGING
    dict_config['loggers'][settings.PACKAGE_NAME]['level'] = settings.LOGLEVEL
    return dict_config


def set_python_logging_config(settings: Settings) -> None:
    """Configure Python logger."""
    logging.config.dictConfig(get_logging_config(settings))
