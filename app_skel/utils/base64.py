# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Base 64 utilities."""

import base64
import typing


def b64encode(data: bytes) -> bytes:
    """Encode data as Base 64 URL safe, stripping padding."""
    return base64.urlsafe_b64encode(data).rstrip(b'=')


def b64decode(data: typing.Union[bytes, str]) -> bytes:
    """Decode data encoded as Base 64 URL safe without padding."""
    if isinstance(data, str):
        data = data.encode()
    return base64.urlsafe_b64decode(data + b'=' * (len(data) % 4))
