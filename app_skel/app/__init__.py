# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""Application initializations."""

# Import app components so that they are executed
from . import events  # noqa: F401
from . import handlers  # noqa: F401
from . import middlewares  # noqa: F401
from . import router  # noqa: F401
from .asgi import app
from .asgi import application

__all__ = (
    'app',
    'application',
)
